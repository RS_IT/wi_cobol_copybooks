      ******************************************************************
      * Omni SC record 
      *
      * #                DATE    PROG  DESCRIPTION
      * W11152         01/21/16  RPS   OAO additions 
      * PRJTASK0131164 04/21/23  rps   Omni upgrade to 7.5
      ******************************************************************
           05  FILE-SYSTEM                    PIC X(10).
           05  AHSC-KEY.                                
DE005          10  AHSC-PLAN-NUM              PIC X(6). 
DE007          10  AHSC-SOURCE                PIC X.    
           05  AHSC-DATA.                               
DE100          10  AHSC-SOURCE-NAME           PIC X(30).
DE150          10  AHSC-SHORT-SOURCE-NAME     PIC X(12).
DE200          10  AHSC-CNTB-CTR-OPT          PIC X.    
DE210          10  AHSC-SUSP-OVERRIDE         PIC X.    
DE220          10  AHSC-SAMEAS-SRC            PIC X.    
DE230          10  AHSC-ELECT-DEF-IND         PIC X.    
DE240          10  AHSC-LISS-CE-ORDER         PIC 9.    
DE245          10  AHSC-LPAY-CE-ORDER         PIC 9.    
DE250          10  AHSC-EXCESS-XFER-SRC       PIC X.    
DE255          10  AHSC-EXCESS-FORFEIT-CNTL   PIC X.    
DE260          10  AHSC-415-LIMIT-IND         PIC 9.    
DE265          10  AHSC-TOP-HEAVY-IND         PIC X.    
DE340          10  AHSC-ROLLOVER-FLG          PIC X.    
DE300          10  AHSC-T114R-FLAG            PIC X.    
DE310          10  AHSC-VR-VOICING-CODE       PIC -9(4).                                            
DE601          10  AHSC-UDF-CODE-01           PIC X.      
DE602          10  AHSC-UDF-CODE-02           PIC X.      
DE603          10  AHSC-UDF-CODE-03           PIC X.      
DE604          10  AHSC-UDF-CODE-04           PIC X.      
DE605          10  AHSC-UDF-CODE-05           PIC X.      
DE606          10  AHSC-UDF-CODE-06           PIC X.      
DE607          10  AHSC-UDF-CODE-07           PIC X.      
DE608          10  AHSC-UDF-CODE-08           PIC X.      
DE609          10  AHSC-UDF-CODE-09           PIC X.      
DE610          10  AHSC-UDF-CODE-10           PIC X.                                            
DE621          10  AHSC-UDF-PERCENT-01        PIC -9.9(4).
DE622          10  AHSC-UDF-PERCENT-02        PIC -9.9(4).
DE623          10  AHSC-UDF-PERCENT-03        PIC -9.9(4).
DE624          10  AHSC-UDF-PERCENT-04        PIC -9.9(4).
DE625          10  AHSC-UDF-PERCENT-05        PIC -9.9(4).
DE275          10  AHSC-LOOK-LIKE-SRC         PIC X.    
DE280          10  AHSC-ROLLED-SRC-TYPE       PIC 9.    
DE229          10  AHSC-ROTH-DEFERRAL         PIC X.    
DE333          10  AHSC-VEST-SCHEDULE         PIC 99.   
DE285          10  AHSC-POST86-AFTERTAX-OPT   PIC X.    
DE290          10  AHSC-IRA-SRC-TYPE          PIC 9.    
DE350          10  AHSC-DIV-PASS-THRU-FLG     PIC X.    
DE360          10  AHSC-DBEN-VEST-IND         PIC -9.   
DE295          10  AHSC-DIST-MIN-AGE          PIC -9(3).9(2). 
DE630          10  AHSC-UDF-1                 PIC X(80).    
DE631          10  AHSC-UDF-2                 PIC X(80).    
DE400          10  AHSC-CNTB-INCL-IN-TEST-CD  PIC X.                    7.5 New
DE405          10  AHSC-ADP-SAFE-HARBOR-IND   PIC X.                    7.5 New
DE410          10  AHSC-ACP-SAFE-HARBOR-IND   PIC X.                    7.5 New
DE415          10  AHSC-QMAC-SHIFT-OPT        PIC X.                    7.5 New
